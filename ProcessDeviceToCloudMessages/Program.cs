﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;

namespace ProcessDeviceToCloudMessages
{
    class Program
    {
        static void Main(string[] args)
        {
            string iotHubConnectionString = "HostName=VHKA1.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=Lk39ZEUxU2wVHkIexZqTkTqDtQi0Wxu4YCbsYbjXXpc=";
            string iotHubD2cEndpoint = "messages/events";
            StoreEventProcessor.StorageConnectionString = "DefaultEndpointsProtocol=https;AccountName=vhka;AccountKey=SXgmp+zqey2jUYhNlvbxgIJk+JW7yv2FbIgzaO4VUT/+Fg+FKgewUTgZa0dF0LhKKpWj/Se+N2j6pENmoQI65g==";
            StoreEventProcessor.ServiceBusConnectionString = "Endpoint=sb://vhka-ns.servicebus.windows.net/;SharedAccessKeyName=send;SharedAccessKey=gzIcbqWdsJ5aUMPkMM9TzZm+ee85AwzZEhRZGeL74rs=;EntityPath=vhka";

            string eventProcessorHostName = Guid.NewGuid().ToString();
            EventProcessorHost eventProcessorHost = new EventProcessorHost(eventProcessorHostName, iotHubD2cEndpoint, EventHubConsumerGroup.DefaultGroupName, iotHubConnectionString, StoreEventProcessor.StorageConnectionString, "messages-events");
            Console.WriteLine("Registering EventProcessor...");
            eventProcessorHost.RegisterEventProcessorAsync<StoreEventProcessor>().Wait();

            Console.WriteLine("Receiving. Press enter key to stop worker.");
            Console.ReadLine();
            eventProcessorHost.UnregisterEventProcessorAsync().Wait();
        }
    }
}
